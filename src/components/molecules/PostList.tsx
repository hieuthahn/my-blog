import { useTrans } from '@/lib';
import { PostCard } from '.';

interface PostListProps {
    posts: any[];
    limit?: number;
    className?: string;
}

const PostList = ({ posts, limit, className }: PostListProps) => {
    const trans = useTrans();
    if (!posts || posts?.length === 0)
        return <span className="text-xs italic text-center">{trans.post.empty}</span>;

    return (
        <div className={`grid grid-cols-1 gap-8 place-items-stretch md:grid-cols-3 ${className}`}>
            {posts?.slice(0, limit)?.map((post: any, index: number) => (
                <PostCard key={index} post={post} />
            ))}
        </div>
    );
};

export default PostList;
